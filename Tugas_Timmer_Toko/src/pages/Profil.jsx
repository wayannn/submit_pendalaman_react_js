import React from 'react'
import NavBar from "../components/NavBar";
import Footer from "../components/Footer";


export default function Profil() {
  return (
    <>
    <NavBar/>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-8 bg-white">
          <article id="profil">
            <h2>Profil</h2>
            <hr />
            <p>
             Lorem Ipsum             
            </p>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ligula est, aliquam id semper ut, efficitur eget neque. Suspendisse et mauris rutrum ante auctor
            </p>
          </article>
        </div>
        <div class="col-sm-4 bg-white">
          <h2 id="skills">History</h2>
          <div class="card-columns">
            <div class="card bg-primary">
              <div class="card-body text-center">
                <p class="card-text">Bag</p>
              </div>
            </div>
            <div class="card bg-primary">
              <div class="card-body text-center">
                <p class="card-text">Hat</p>
              </div>
            </div>
            <div class="card bg-primary">
              <div class="card-body text-center">
                <p class="card-text">Shore</p>
              </div>
            </div>
            <div class="card bg-warning">
              <div class="card-body text-center">
                <p class="card-text">Pants</p>
              </div>
            </div>
            <div class="card bg-success">
              <div class="card-body text-center">
                <p class="card-text">Clothes</p>
              </div>
            </div>
            <div class="card bg-danger">
              <div class="card-body text-center">
                <p class="card-text">Laptop</p>
              </div>
            </div>
            <div class="card bg-light">
              <div class="card-body text-center">
                <p class="card-text">Sock</p>
              </div>
            </div>
            <div class="card bg-warning">
              <div class="card-body text-center">
                <p class="card-text">Headphone</p>
              </div>
            </div>
            <div class="card bg-info">
              <div class="card-body text-center">
                <p class="card-text">Food</p>
              </div>
            </div>
          </div>
          <h2>Language</h2>
          <table class="table table-hover">
            <tr>
              <td>Indonesian Language</td>
            </tr>
            <tr>
              <td>English</td>
            </tr>
            <tr>
              <td>Nihongo</td>
            </tr>
          </table>
        </div>
      </div>
      <hr />
      <div class="container-fluid form">
        <form action="/action_page.php">
          <h4 class="message">Give me some messages</h4>
          <div class="form-group">
            <label for="email">First Name : </label>
            <input
              type="email"
              class="form-control"
              id="email"
              placeholder="Enter your first name "
            />
          </div>
          <div class="form-group">
            <label for="email">Last Name : </label>
            <input
              type="email"
              class="form-control"
              id="email"
              placeholder="Enter your last name"
            />
          </div>
          <div class="form-group">
            <label for="email">Email : </label>
            <input
              type="email"
              class="form-control"
              id="email"
              placeholder="Enter your email"
            />
          </div>
          <div class="form-group">
            <label for="pwd">Messages : </label>
            <textarea
              type="text"
              class="form-control"
              id="pwd"
              placeholder="Enter your password"
            ></textarea>
          </div>
          <div class="row float-right align-items-center">
            <button type="submit" class="btn btn-primary mb-2">Submit</button>
          </div>
        </form>
      </div>
    </div>
    <Footer/>
    </>
  )
}
