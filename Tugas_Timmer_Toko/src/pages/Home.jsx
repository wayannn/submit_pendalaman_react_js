import React, { Component } from 'react'
import NavBar from "../components/NavBar";
import Footer from "../components/Footer";


export default class Home extends Component {
   constructor() {
    super();
    this.state = {
      length: 0,
      products: [],
    };
  }

  componentDidMount() {
    fetch("https://fakestoreapi.com/products?limit=9")
      .then((res) => res.json())
      .then((res) =>
        this.setState({
          length: res.length,
          products: res,
        })
      );
  }
 
  render() {
    const { products } = this.state;
    const listProduct = products.map((itemProduct) => (
      <div class="col-md-4 my-5">
        <div class="card">
          <img src={itemProduct.image} class="card-img-top" alt="gambar" />
          <div class="card-body">
            <h5 class="card-title">{itemProduct.title}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${itemProduct.price}</h6>
            <br></br>
            <a href="#" class="btn btn-primary">
              Buy
            </a>
          </div>
        </div>
      </div>
    ));
    return (
      <>
      <NavBar/>
      <div className="container">
        <div className="row  mt-5">
          <div className="col">
            <div className="mt-5">
              <h2 className="text-center">
                <u>Our Catalogs</u>
              </h2>
              <div class="row">{listProduct}</div>;
            </div>
          </div>
        </div>
      </div>
      <Footer/>
      </>
    )
  }
}
