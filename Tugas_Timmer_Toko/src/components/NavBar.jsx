import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class NavBar extends Component {
  render() {
    return (
      <nav class="navbar navbar-expand-lg navbar-dark bg-primary static-top">
        <div class="container">
          <a class="navbar-brand" href="#">
            <p>Wellcome to WayMart !</p>
          </a>
          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarResponsive"
            aria-controls="navbarResponsive"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <Link exact to="/" class="nav-link">
                  Home
                </Link>
              </li>
              <li class="nav-item">
                <Link exact to="/about" class="nav-link">
                  About
                </Link>
              </li>
              <li class="nav-item">
                <Link exact to="/login" class="nav-link">
                  Logout
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}
