import React from "react";

export default function Footer() {
  return (
    <footer class="page-footer font-small blue bg-primary">
      <div class="footer-copyright text-center  text-white py-3">
        © 2020 Copyright:
        <a> I Wayan Agus Aditya Marantika</a>
      </div>
    </footer>
  );
}
